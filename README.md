# tiny



## Getting started

Download the project, compile tiny.c - it only uses standard libraries so you should not have 
any dependancy problems. Put the resulting executable in your favorite place on the path.

Read the esolang page: https://esolangs.org/wiki/Tiny

Create a source file in your favorite editor don't forget you can put #! /path/to/tiny  as the first line
(with no line number)

Or edit your program in tiny!

